<?php 
$app->get('/', function() use($app) { 
$datos = array( 
'titulo' => 'Poblacion, Hogares y Viviendas', 
'recursos' => array( 
	array('url' => '/api', 'descripcion' => 'Este documento HTML'),
	array('url' => '/api/entidades', 'descripcion' => 'Un documento XML'), 
	) ); 
	$app->render('root.php', $datos); 
});
